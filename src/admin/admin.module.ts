import { Module, UseGuards } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { from } from 'rxjs';
import { AuthService } from 'src/auth/auth.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { JwtStrategy } from 'src/auth/jwt.strategy';
import { RolesGuard } from 'src/auth/roles.guard';
import { BlogService } from 'src/blog/blog.service';
import { BlogEntity } from 'src/entities/blog.entity';
import { CommentEntity } from 'src/entities/comment.entity';
import { PageEntity } from 'src/entities/page.entity';
import { UserEntity } from 'src/entities/user.entity';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { PagesService } from '../pages/pages.service'
import { AppService } from 'src/app.service';
import { UserGuard } from 'src/auth/user.guard';
@Module({
  imports: [
    TypeOrmModule.forFeature([
      BlogEntity,
      UserEntity,
      CommentEntity,
      PageEntity
    ]),
    JwtModule.register({
      secret: 'thisIsASecretKey',
      signOptions: {
        expiresIn: 14400,
      },
    }),
    PassportModule.register({ defaultStrategy: 'jwt' }),
  ],
  controllers: [AdminController],
  exports: [PassportModule, JwtStrategy, AuthService],
  providers: [AdminService,AppService, BlogService, PagesService ,AuthService, JwtStrategy, RolesGuard, JwtAuthGuard, UserGuard]
})
export class AdminModule {}
