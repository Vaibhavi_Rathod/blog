import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { BlogEntity } from "src/entities/blog.entity";
import { CommentEntity } from "src/entities/comment.entity";
import { UserEntity } from "src/entities/user.entity";
import { Repository } from "typeorm";
import * as moment from "moment";

@Injectable()
export class AdminService {
  constructor(
    @InjectRepository(BlogEntity)
    private BlogRepo: Repository<BlogEntity>,
    @InjectRepository(UserEntity)
    private userRepo: Repository<UserEntity>,
    @InjectRepository(CommentEntity)
    private commentRepo: Repository<CommentEntity>
  ) {}

  async findAllUsers(user: UserEntity): Promise<any> {
    const users = await this.userRepo.find({
      where: { isAdmin : false },
      order: {
        id: "DESC"
      }
    });
    return users;
  }

  async findLatestUsers(user: UserEntity): Promise<any> {
    let currentDate = moment(new Date()).toDate();
      let pastDate = moment(new Date()).toDate();

      pastDate = moment().subtract(7, 'days').toDate();
      const users = await this.userRepo.createQueryBuilder('users')
      .where(
        'DATE(createdAt) BETWEEN DATE(:pastDate) AND DATE(:currentDate)',
        {
          pastDate: pastDate,
          currentDate: currentDate,
        },
      )
      .orderBy('id', 'DESC')
      .getRawMany();
   return users ;
  }
}
