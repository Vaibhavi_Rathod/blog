import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Redirect,
  Render,
  Res,
  Response,
  UnauthorizedException,
  UseFilters,
  UseGuards,
  ValidationPipe,
} from "@nestjs/common";
import { v4 } from "uuid";
import { User } from "src/auth/user.decorator";
import { UserEntity } from "src/entities/user.entity";
import { AdminService } from "./admin.service";
import { BlogService } from "../blog/blog.service";
import * as moment from "moment";
import { CreateBlogDTO, UpdateBlogDTO } from "src/models/blog.models";
import { LoginDTO, RegisterDTO } from "src/models/user.model";
import { AuthService } from "../auth/auth.service";
import { ApiBearerAuth } from "@nestjs/swagger";
import { RolesGuard } from "src/auth/roles.guard";
import * as bcrypt from "bcrypt";
import { PagesService } from "src/pages/pages.service";
import { CreatePageDTO } from "src/models/page.models";

import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { mail } from "../mail";
import { ViewAuthFilter } from "src/auth/http-exception.filter";

@Controller("admin")
export class AdminController {
  constructor(
    private BlogService: BlogService,
    private AdminService: AdminService,
    private AuthService: AuthService,
    private PagesService: PagesService,
    @InjectRepository(UserEntity) private userRepo: Repository<UserEntity>
  ) {}

  // Get Login Page
  @Get("/login")
  @Render("login.hbs")
  getlogin(@Res() res) {
    res.clearCookie("token");
  }

  @Get("/user/login")
  @Render("user login.hbs")
  async getuserlogin(@Res() res) {
    res.clearCookie("usertoken");
  }

  // Pass Login Data and generate token
  @Post("/login")
  async login(
    @Body(ValidationPipe) credentials: LoginDTO,
    @Res() res
  ): Promise<any> {
    const user = await this.AuthService.login(credentials, res);
    if (user) {
      res.cookie("token", user.token, {
        // maxAge: 140000, // Lifetime
      });
      res.redirect("/admin/index");
    }
  }

  @Post("/user/login")
  async userLogin(
    @Body(ValidationPipe) credentials: LoginDTO,
    @Res() res
  ): Promise<any> {
    const user = await this.userRepo.findOne({
      where: { email: credentials.email },
    });
    if (user && user.verified === false) {
      user.userID == credentials.password
        ? res.render("resetPassword.hbs")
        : res.render("exception.hbs", {
            message: "Invalid Password",
            status: 400,
          });
    } else if (user && user.verified === true) {
      const loginUser = await this.AuthService.login(credentials, res);
      if (loginUser) {
        res.cookie("usertoken", loginUser.token, {
          // maxAge: 140000, // Lifetime
        });
        res.redirect("/home");
      }
    } else if (!user) {
      return res.render("exception.hbs", {
        message: "Invalid Email Address",
        status: 400,
      });
    }
  }

  // Get home page of admin with bearer Token and authGuard
  // @ApiBearerAuth()
  @Get("/index")
  @Render("index.hbs")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async root(@User() user: UserEntity) {
    const userCount = await this.AdminService.findAllUsers(user);
    const blogs = await this.BlogService.findAll(user);
    const users = await this.AdminService.findLatestUsers(user);
    const pages = await this.PagesService.findAllPages();
    const mapArray = users.map((users) => {
      const createdAt = moment(users.users_createdAt).format("DD-MM-YYYY");
      users.createdAt = createdAt;
    });
    return {
      users: users,
      blogCount: blogs.length,
      userCount: userCount.length,
      pageCount: pages.length,
    };
  }

  // Get pages and page page
  @Get("/pages")
  @Render("pages.hbs")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async pages(@User() user: UserEntity) {
    const userCount = await this.AdminService.findAllUsers(user);
    const blogs = await this.BlogService.findAll(user);
    const pages = await this.PagesService.findAllPages();
    return {
      pages: pages,
      pageCount: pages.length,
      blogCount: blogs.length,
      userCount: userCount.length,
    };
  }

  // post req for create page and redirect to page-page (to get create page we have model)
  @Post("/done/page")
  @Redirect("/admin/pages")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async forCreatePage(@Body() data: CreatePageDTO): Promise<any> {
    if (data.published) {
      data.published = true;
    }
    await this.PagesService.createPage(data);
  }

  // delete page by uuid and redirect to page-page
  @Get("/page/delete/:uuid")
  @Redirect("/admin/pages")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async deletePage(@Param("uuid") uuid: string): Promise<any> {
    await this.PagesService.deletePage(uuid);
  }

  // Get posts and post page
  @Get("/posts")
  @Render("posts.hbs")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async posts(@User() user: UserEntity) {
    const userCount = await this.AdminService.findAllUsers(user);
    const blogs = await this.BlogService.findAll(user);
    const pages = await this.PagesService.findAllPages();
    const mapArray = blogs.map((blogs) => {
      const createdAt = moment(blogs.createdAt).format("DD-MM-YYYY");
      blogs.createdAt = createdAt;
    });
    return {
      blogs: blogs,
      blogCount: blogs.length,
      userCount: userCount.length,
      pageCount: pages.length,
    };
  }

  // for finding specific post
  @Get("/post/:slug")
  @Render("dynamicPost.hbs")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async getSpecificPost(@Param("slug") slug: string): Promise<any> {
    const page = await this.BlogService.findBySlug(slug);
    return { page: page };
  }

  // for create a blog redirect to addblog page which have form
  @Get("/create/blog")
  @Render("addBlog.hbs")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async createBlog(@User() user: UserEntity) {
    const userCount = await this.AdminService.findAllUsers(user);
    const blogs = await this.BlogService.findAll(user);
    const pages = await this.PagesService.findAllPages();
    return {
      blogCount: blogs.length,
      userCount: userCount.length,
      pageCount: pages.length,
    };
  }

  // post add blog form for create a blog and redirect to post page
  @Post("/create/blog")
  @Redirect("/admin/posts")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async forCreateBlog(
    @User() user: UserEntity,
    @Body() data: CreateBlogDTO
  ): Promise<any> {
    if (data.published) {
      data.published = true;
    }
    await this.BlogService.createBlog(user, data);
  }

  // for edit blog : get slug and edit page
  @Get("/edit/:slug")
  @Render("edit.hbs")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async editBlog(
    @Param("slug") slug: string,
    @User() user: UserEntity
  ): Promise<any> {
    const data = await this.BlogService.findBySingleSlug(user, slug);
    const users = await this.AdminService.findAllUsers(user);
    const blogs = await this.BlogService.findAll(user);
    const pages = await this.PagesService.findAllPages();
    return {
      blogCount: blogs.length,
      userCount: users.length,
      pageCount: pages.length,
      data: data,
    };
  }

  // pass updated dataform and redirect to post page
  @Post("/update/:slug") // user: undefined
  @Redirect("/admin/posts")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async updateBlog(
    @Param("slug") slug: string,
    @User() user: UserEntity,
    @Body() data: UpdateBlogDTO
  ): Promise<any> {
    if (data.published) {
      data.published = true;
    } else {
      data.published = false;
    }
    await this.BlogService.updateBlog(slug, user, data);
  }

  // delete blog by slug and redirect to post page
  @Get("/delete/:slug")
  @Redirect("/admin/posts")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async deleteBlog(
    @Param("slug") slug: string,
    @User() user: UserEntity
  ): Promise<any> {
    await this.BlogService.deleteBlog(slug, user);
  }

  // get users and user page
  @Get("/users")
  @Render("users.hbs")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async users(@User() user: UserEntity) {
    const users = await this.AdminService.findAllUsers(user);
    const blogs = await this.BlogService.findAll(user);
    const pages = await this.PagesService.findAllPages();
    const mapArray = users.map((users) => {
      const createdAt = moment(users.createdAt).format("DD-MM-YYYY");
      users.createdAt = createdAt;
    });
    return {
      users: users,
      blogCount: blogs.length,
      userCount: users.length,
      pageCount: pages.length,
    };
  }

  // to create user redirect to addUser page which have add user form
  @Get("/create/user")
  @Render("addUser.hbs")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  async createuser(@User() user: UserEntity) {
    const userCount = await this.AdminService.findAllUsers(user);
    const blogs = await this.BlogService.findAll(user);
    const pages = await this.PagesService.findAllPages();
    return {
      blogCount: blogs.length,
      userCount: userCount.length,
      pageCount: pages.length,
    };
  }

  // exception page
  @Get("/user/notfound")
  @Render("exception.hbs")
  notfound() {}

  @Post("/create/user")
  @UseGuards(RolesGuard)
  @UseFilters(ViewAuthFilter)
  // @Render("reset.hbs") // user: undefined
  async forCreateUser(
    @Body() credentials: RegisterDTO,
    @Res() res,
    @User() user: UserEntity
  ): Promise<any> {
    const Data = await this.userRepo.findOne({
      where: { email: credentials.email },
    });
    if (!Data) {
      const id = v4();
      credentials.userID = id;
      await mail(credentials.email, credentials.username, credentials.userID);
      await this.AuthService.register(credentials, res);
      return res.redirect("/admin/users");
    } else {
      return res.render("exception.hbs", {
        message: "Already Added",
        status: 400,
      });
    }
  }

  @Post("/user/resetPassword")
  async setPassword(@Body() Data: any, @Res() res): Promise<any> {
    const user = await this.userRepo.findOne({
      where: { email: Data.email },
    });
    if (user && user.verified === false) {
      if (Data.password1 == Data.password2 && Data.password1.length >= 8) {
        const hash = await bcrypt.hash(Data.password1, 10);
        user.password = hash;
        user.verified = true;
        await user.save();
        const email = user.email;
        const password = Data.password1;
        const data = await this.AuthService.login({ email, password }, res);
        if (data) {
          res.cookie("usertoken", data.token, {
            // maxAge: 140000, // Lifetime
          });
          return res.redirect("/home");
        }
      } else {
        return res.render("exception.hbs", {
          message: "Both password doesnot match",
          status: 400,
        });
      }
    } 
    else if(user && user.verified === true) {
      return res.render("exception.hbs", {
        message: "already Reset",
        status: 400,
      });
    }else if (!user) {
      return res.render("exception.hbs", {
        message: "Invalid email Address",
        status: 400,
      });
    } 
  }
}
