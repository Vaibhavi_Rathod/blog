import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { PageEntity } from "src/entities/page.entity";
import { CreatePageDTO, UpdatePageDTO } from "src/models/page.models";
import { Repository } from "typeorm";

@Injectable()
export class PagesService {
  constructor(
    @InjectRepository(PageEntity)
    private PageRepo: Repository<PageEntity>
  ) {}

  async findAllPages(): Promise<any[]> {
    return await this.PageRepo.find({ where: {} });
  }

  async publishedPages(): Promise<any[]> {
    return await this.PageRepo.find({ where: { published: true } });
  }

  findByuuid(uuid: string): Promise<PageEntity> {
    return this.PageRepo.findOne({
      where: { uuid },
    });
  }
  
  async createPage(data: CreatePageDTO): Promise<any> {
    try {
      const page = this.PageRepo.create(data);
      await page.save();
    } catch (error) {
      throw new BadRequestException(error.message, error.status);
    }
  }

  async updatePage(uuid: string, data: UpdatePageDTO): Promise<any> {
    try {
      await this.PageRepo.update({ uuid }, data);
      const page = await this.findAllPages();
      return page;
    } catch (error) {
      throw new BadRequestException(error.message, error.status);
    }
  }

  async deletePage(uuid: string): Promise<any> {
    try {
      const page = await this.findByuuid(uuid);
      await this.PageRepo.remove(page);
      const pages = await this.findAllPages();
      return pages;
    } catch (error) {
      throw new BadRequestException(error.message, error.status);
    }
  }
}
