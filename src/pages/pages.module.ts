import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PageEntity } from 'src/entities/page.entity';
import { PagesController } from './pages.controller';
import { PagesService } from './pages.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PageEntity
    ])
  ],
  controllers: [PagesController],
  providers: [PagesService]
})
export class PagesModule {}
