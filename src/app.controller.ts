import { Controller, Get, Param, Render, UseFilters, UseGuards } from "@nestjs/common";
import { AppService } from "./app.service";
import { ViewAuthFilter } from "./auth/http-exception.filter";
import { UserGuard } from "./auth/user.guard";
import { PagesService } from './pages/pages.service';
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly PagesService: PagesService) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  @Get('/home')
  @Render('home.hbs')
  @UseGuards(UserGuard)
  @UseFilters(ViewAuthFilter)
  async home(): Promise<any> {
    const pages = await this.PagesService.publishedPages();
    return{ pages: pages}
  }

  @Get('/home/:uuid')
  @Render('dynamicPage.hbs')
  @UseGuards(UserGuard)
  @UseFilters(ViewAuthFilter)
  async getpages(@Param("uuid") uuid: string,): Promise<any> {
    const pages = await this.PagesService.publishedPages();
    const page = await this.PagesService.findByuuid(uuid);
    return { page: page, pages: pages}
  }
}
