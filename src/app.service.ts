import { Injectable } from "@nestjs/common";
import { MailerService } from '@nestjs-modules/mailer';
import { RegisterDTO } from "./models/user.model";

@Injectable()
export class AppService {
  getHello(): string {
    return "Hello World!";
  }
}
