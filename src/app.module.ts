import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { BlogModule } from './blog/blog.module';
// import { DatabaseConnectionService } from 'database-connection.service';
import { AdminModule } from './admin/admin.module';
import { PagesModule } from './pages/pages.module';
import { PagesService } from './pages/pages.service';
import { PageEntity } from './entities/page.entity';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { UserEntity } from './entities/user.entity';
import { UserGuard } from './auth/user.guard';
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    TypeOrmModule.forFeature([PageEntity,UserEntity]),
    AuthModule,
    BlogModule,
    AdminModule,
    PagesModule,
    // UserEntity
  ],
  controllers: [AppController],
  providers: [AppService, PagesService, UserGuard],
})
export class AppModule {}