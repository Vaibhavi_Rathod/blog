import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
  UsePipes,
} from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { BlogEntity } from "src/entities/blog.entity";
import { UserEntity } from "src/entities/user.entity";
import {
  CreateBlogDTO,
  CreatePaymentDTO,
  UpdateBlogDTO,
} from "src/models/blog.models";
import { CommentResponse, CreateCommentDTO } from "src/models/comment.models";
import { CommentEntity } from "src/entities/comment.entity";
import Stripe from "stripe";
import { use } from "passport";

const stripe = new Stripe(
  "sk_test_51IQRLbH483mbeU4KWrFCuBEvHfbdJeXQD6rf7qS0ouykf9ef30PJ2gKlBXEhcvg8XH6Okts213VwhlY4ENc6MSsl006WDK7IFm",
  {
    apiVersion: null,
  }
);
@Injectable()
export class BlogService {
  constructor(
    @InjectRepository(BlogEntity)
    private BlogRepo: Repository<BlogEntity>,
    @InjectRepository(UserEntity)
    private userRepo: Repository<UserEntity>,
    @InjectRepository(CommentEntity)
    private commentRepo: Repository<CommentEntity>
  ) {}

  async findAll(user: UserEntity): Promise<any[]> {
    // let findOptions = null;
    // if (user.payment_successful === false) {
    //   let option: any = {
    //     where: { is_build: false },
    //   };
    //   findOptions = option;
    // }
    // return (await this.BlogRepo.find(findOptions)).map((Blog) =>
    //   Blog.toBlog(user)
    // );
    return (await this.BlogRepo.find()).map((Blog) => Blog.toBlog(user));
  }

  findBySlug(slug: string): Promise<BlogEntity> {
    return this.BlogRepo.findOne({
      where: { slug },
    });
  }

  findBySingleSlug(user: UserEntity, slug: string): Promise<BlogEntity> {
    // if (user.payment_successful === false) {
    //   return this.BlogRepo.findOne({
    //     where: {
    //       slug,
    //       is_build: false,
    //     },
    //   });
    // } else {
      return this.BlogRepo.findOne({
        where: { slug },
      });
    }
  // }

  private ensureOwnership(user: UserEntity, Blog: BlogEntity): boolean {
    return Blog.author.id === user.id;
  }

  async createBlog(user: UserEntity, data: CreateBlogDTO): Promise<any> {
    try {
      const blog = this.BlogRepo.create(data);
      // blog.author = user;
      const { slug } = await blog.save();
      const blogs = await this.findAll(user);
      return blogs ;
    } catch (error) {
      throw new BadRequestException(error.message, error.status);
    }
  }

  async updateBlog(
    slug: string,
    user: UserEntity,
    data: UpdateBlogDTO
  ): Promise<any> {
    try {
      // const Blog = await this.findBySlug(slug);
      // if (!this.ensureOwnership(user, Blog)) {
      //   throw new UnauthorizedException();
      // }
      await this.BlogRepo.update({ slug }, data);
      const blogs = await this.findAll(user);
      return blogs ;
    } catch (error) {
      throw new BadRequestException(error.message, error.status);
    }
  }

  async deleteBlog(slug: string, user: UserEntity): Promise<any> {
    try {
      const Blog = await this.findBySlug(slug);
      // if (!this.ensureOwnership(user, Blog)) {
      //   throw new UnauthorizedException();
      // }
      await this.BlogRepo.remove(Blog);
      // return Blog.toBlog(user);
      const blogs = await this.findAll(user);
      return blogs ;
    }catch (error) {
      throw new BadRequestException(error.message, error.status);
    }
  }

  async findByBlogSlug(slug: string): Promise<any> {
    const blog = await this.findBySlug(slug);
    const comments = blog.comments;
    return comments
  }

  async createComment(
    slug: string,
    user: UserEntity,
    data: CreateCommentDTO
  ): Promise<CommentResponse> {
    try {
      const blog = await this.findBySlug(slug);
      const comment = this.commentRepo.create(data);
      comment.author = user;
      comment.blog = blog;
      blog.comments = comment;
      await comment.save();
      
      return this.commentRepo.findOne({ 
        where: { 
          comment_body: data.comment_body,
          author: user
       } });

    } catch (error) {
      throw new BadRequestException(error.message, error.status);
    }
  }

  async likeBlog(slug: string, user: UserEntity): Promise<any> {
    const Blog = await this.findBySlug(slug);
    Blog.likedBy.push(user);
    await Blog.save();
    return (await this.findBySlug(slug)).toBlog(user);
  }

  async unlikeBlog(slug: string, user: UserEntity): Promise<any> {
    const Blog = await this.findBySlug(slug);
    Blog.likedBy = Blog.likedBy.filter((fav) => fav.id !== user.id);
    await Blog.save();
    return (await this.findBySlug(slug)).toBlog(user);
  }

  async payment(user: UserEntity, data: CreatePaymentDTO): Promise<any> {
    try {
      if (user.payment_successful === true) {
        throw new BadRequestException("already done");
      }
      const cardToken = await stripe.tokens.create({
        card: {
          number: data.number,
          exp_month: data.exp_month,
          exp_year: data.exp_year,
          cvc: data.cvc,
        },
      });

      await stripe.customers.createSource(user.customer_id, {
        source: cardToken.id,
      });

      const charge = await stripe.charges.create({
        amount: 200,
        currency: "inr",
        customer: user.customer_id,
      });
      user.payment_successful = true;
      await user.save();
    } catch (error) {
      throw new HttpException(error.response, error.status);
    }
    const status = "PAYMENT SUCCESSFUl";
    const date = new Date();
    return { status, date };
  }
}
