import {
  Controller,
  Get,
  Param,
  Post,
  UseGuards,
  Body,
  ValidationPipe,
  Put,
  Delete,
  BadRequestException,
  HttpException,
} from "@nestjs/common";
import { ApiBearerAuth, ApiUnauthorizedResponse } from "@nestjs/swagger";

import { CommentResponse, CreateCommentDTO } from "src/models/comment.models";
import { UserEntity } from "src/entities/user.entity";
import {
  CreateBlogDTO,
  CreatePaymentDTO,
  UpdateBlogDTO,
} from "src/models/blog.models";
import { BlogService } from "./blog.service";
import { ResponseObject } from "src/models/response.model";
import { User } from "src/auth/user.decorator";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";
import { RolesGuard } from "src/auth/roles.guard";
import { JwtStrategy } from "src/auth/jwt.strategy";
@Controller("blogs")
export class BlogController {
  constructor(private BlogService: BlogService) {}

  // LIST ALL BLOGS
  @ApiBearerAuth()
  @Get()
  @UseGuards(JwtStrategy)
  async findAll(
    @User() user: UserEntity
  ): Promise<
    ResponseObject<"blogs", any[]> & ResponseObject<"BlogsCount", number>
  > {
    try {
      const blogs = await this.BlogService.findAll(user);
      if (blogs.length == 0) {
        throw new BadRequestException("NO BLOG EXITS");
      }
      return {
        blogs,
        BlogsCount: blogs.length,
      };
    } catch (error) {
      throw new HttpException(error, error.status);
    }
  }

  // GET SINGLE BLOG
  @ApiBearerAuth()
  @Get("/:slug")
  @UseGuards(JwtAuthGuard)
  async findOne(
    @User() user: UserEntity,
    @Param("slug") slug: string
  ): Promise<any> {
    try {
      const blog = await this.BlogService.findBySingleSlug(user, slug);
      if (!blog) {
        throw new BadRequestException("NO BLOG EXITS");
      }
      return { blog };
    } catch (error) {
      throw new HttpException(error, error.status);
    }
  }

  // CREATE A BLOG
  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  async createBlog(
    @User() user: UserEntity,
    @Body() data: CreateBlogDTO
  ): Promise<any> {
    const blog = await this.BlogService.createBlog(user, data);
    return { blog };
  }

  // UPDATE A BLOG
  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @Put("/:slug")
  @UseGuards(JwtAuthGuard)
  async updateBlog(
    @Param("slug") slug: string,
    @User() user: UserEntity,
    @Body() data: UpdateBlogDTO
  ): Promise<any> {
    const blog = await this.BlogService.updateBlog(slug, user, data);
    return { blog };
  }

  // DELETE A BLOG
  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @Delete("/:slug")
  @UseGuards(JwtAuthGuard)
  async deleteBlog(
    @Param("slug") slug: string,
    @User() user: UserEntity
  ): Promise<any> {
    const blog = await this.BlogService.deleteBlog(slug, user);
    return { blog };
  }

  // LIST BLOG COMMENTS
  @Get('/:slug/comments')
  async findComments(
    @Param('slug') slug: string,
  ): Promise<any> {
    const comments = await this.BlogService.findByBlogSlug(slug);
    return { comments };
  }

  // CREATE NEW COMMENT
  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @Post("/:slug/comments")
  @UseGuards(JwtAuthGuard)
  async createComment(
    @Param("slug") slug: string,
    @User() user: UserEntity,
    @Body(ValidationPipe) data: CreateCommentDTO
  ): Promise<ResponseObject<'comment', CommentResponse>> {
    const comment = await this.BlogService.createComment(slug, user, data);
    return { comment };
  }

  // LIKE BLOG
  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @Post("/:slug/like")
  @UseGuards(JwtAuthGuard)
  async likeBlog(
    @Param("slug") slug: string,
    @User() user: UserEntity
  ): Promise<any> {
    const blog = await this.BlogService.likeBlog(slug, user);
    return { blog };
  }

  // UNLIKE BLOG
  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @Delete("/:slug/like")
  @UseGuards(JwtAuthGuard)
  async unlikeBlog(
    @Param("slug") slug: string,
    @User() user: UserEntity
  ): Promise<any> {
    const blog = await this.BlogService.unlikeBlog(slug, user);
    return { blog };
  }

  // PAYMENT
  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @Post("/payment")
  @UseGuards(JwtAuthGuard, RolesGuard)
  async payment(
    @User() user: UserEntity,
    @Body() data: CreatePaymentDTO
  ): Promise<any> {
    const payment = await this.BlogService.payment(user, data);
    return { payment };
  }
}
