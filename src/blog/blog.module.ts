import { Module } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogController } from './blog.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogEntity } from 'src/entities/blog.entity';
import { UserEntity } from 'src/entities/user.entity';
import { AuthModule } from 'src/auth/auth.module';
import { CommentEntity } from 'src/entities/comment.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BlogEntity,
      UserEntity,
      CommentEntity
    ]),
    AuthModule,
  ],
  providers: [BlogService],
  controllers: [BlogController],
})
export class BlogModule {}