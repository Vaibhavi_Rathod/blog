import { IsString, IsArray, IsOptional, IsBoolean } from 'class-validator';

export class CreatePageDTO {
  @IsString()
  title: string;

  @IsString()
  editor1: string;

  @IsString()
  @IsOptional()
  published: boolean;
}

export class UpdatePageDTO {
  @IsString()
  @IsOptional()
  title: string;

  @IsString()
  @IsOptional()
  editor1: string;

  @IsString()
  @IsOptional()
  published: boolean;
}