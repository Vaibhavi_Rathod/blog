import { IsString } from 'class-validator';

import { UserEntity } from 'src/entities/user.entity';

export class CreateCommentDTO {
  @IsString()
  comment_body: string;
}

export class CommentResponse {
  id: number;
  createdAt: string | Date;
  updatedAt: string | Date;
  comment_body: string;
  author: UserEntity;
}