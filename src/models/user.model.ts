import { Optional } from '@nestjs/common';
import {
  IsEmail,
  IsString,
  MinLength,
  MaxLength,
  IsOptional,
  IsBoolean,
  UUIDVersion,
  IsUUID,
} from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';

export class LoginDTO {
  @IsEmail()
  @IsString()
  @MinLength(4)
  email: string;

  @IsString()
  @MinLength(8)
  password: string;
}

export class LoginBody {
  user: LoginDTO;
}

export class RegisterDTO extends PartialType(LoginDTO) {
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  username: string;

  @IsString()
  @IsOptional()
  isAdmin: boolean;

  @IsString()
  @IsOptional()
  userID: string;
}

export class RegisterBody {
  user: RegisterDTO;
}

export interface AuthPayload {
  username: string;
}

export interface UserResponse {
  email: string;
  username?: string;
}

export interface AuthResponse extends UserResponse {
  token: string;
}


