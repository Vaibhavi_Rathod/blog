import { IsString, IsArray, IsOptional, IsBoolean } from 'class-validator';

export class CreateBlogDTO {
  @IsString()
  title: string;

  @IsString()
  editor1: string;

  @IsString()
  description: string;

  @IsString()
  @IsOptional()
  published: boolean;
}

export class UpdateBlogDTO {
  @IsString()
  @IsOptional()
  title: string;

  @IsString()
  @IsOptional()
  editor1: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  @IsOptional()
  published: boolean;
}

export class CreatePaymentDTO {
  @IsString()
  number: string;

  @IsString()
  exp_month: string;

  @IsString()
  exp_year: string;

  @IsString()
  cvc: string
}
