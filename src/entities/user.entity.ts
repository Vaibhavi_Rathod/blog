import {
    Entity,
    Column,
    BeforeInsert,
    JoinTable,
    ManyToMany,
    OneToMany,
  } from 'typeorm';
  import * as bcrypt from 'bcryptjs';
  import { classToPlain, Exclude } from 'class-transformer';
  import { IsEmail } from 'class-validator';
  
  import { AbstractEntity } from './abstract-entity';
  import { BlogEntity } from './blog.entity';
  import { CommentEntity } from './comment.entity';
  import { UserResponse } from 'src/models/user.model';

  @Entity('users')
  export class UserEntity extends AbstractEntity {

    @Column({ default: "" })
    userID: string;

    @Column()
    customer_id: string;

    @Column({ default: false })
    payment_successful: boolean;

    @Column({ unique: true })
    @IsEmail()
    email: string;
  
    @Column()
    username: string;

    @Column({ default: false })
    isAdmin: boolean
  
    @Column({ default: false })
    verified: boolean

    @Column()
    @Exclude()
    password: string;
  
    @OneToMany(
      type => BlogEntity,
      Blog => Blog.author,
    )
    Blogs: BlogEntity[];
  
    @OneToMany(
      type => CommentEntity,
      comment => comment.author,
    )
    comments: CommentEntity[];
  
    @ManyToMany(
      type => BlogEntity,
      Blog => Blog.likedBy,
    )
    likes: BlogEntity[];
      verified_at: any;
  
    async comparePassword(attempt: string) {
      return await bcrypt.compare(attempt, this.password);
    }
  
    toJSON(): UserResponse {
      return <UserResponse>classToPlain(this);
    }
  }