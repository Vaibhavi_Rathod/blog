import {
    BaseEntity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    Entity,
    Column,
  } from 'typeorm';
  
  export abstract class AbstractEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;
  }

  @Entity('all')
export class allInOneEntity {
  @Column()
  pages: number;

  @Column()
  posts: number;

  @Column()
  users: number;
}