import { Entity, Column, ManyToOne } from 'typeorm';
import { classToPlain } from 'class-transformer';

import { AbstractEntity } from './abstract-entity';
import { UserEntity } from './user.entity';
import { BlogEntity } from './blog.entity';
import { CommentResponse } from 'src/models/comment.models';

@Entity('comments')
export class CommentEntity extends AbstractEntity {
  @Column()
  comment_body: string;

  @ManyToOne(
    type => UserEntity,
    user => user.comments,
    { eager: true },
  )
  author: UserEntity;

  @ManyToOne(
    type => BlogEntity,
    blog => blog.comments,
  )
  blog: BlogEntity;

  toJSON() {
    return <CommentResponse>classToPlain(this);
  }
}