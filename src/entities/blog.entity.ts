import {
    Entity,
    Column,
    BeforeInsert,
    ManyToOne,
    ManyToMany,
    RelationCount,
    JoinTable,
    OneToMany,
  } from 'typeorm';
  import { classToPlain } from 'class-transformer';
  import * as slugify from 'slug';
  
  import { AbstractEntity } from './abstract-entity';
  import { UserEntity } from './user.entity';
  import { CommentEntity } from './comment.entity';
import { CheckMetadata } from 'typeorm/metadata/CheckMetadata';
  
  @Entity('Blogs')
  export class BlogEntity extends AbstractEntity {
    @Column()
    slug: string;
  
    @Column()
    title: string;
  
    @Column({type: 'longtext'})
    description: string;
  
    @Column({type: 'longtext'})
    editor1: string;

    @Column({default: false})
    published: boolean;
  
    @ManyToMany(
      type => UserEntity,
      user => user.likes,
      { eager: true },
    )
    @JoinTable()
    likedBy: UserEntity[];
  
    @RelationCount((Blog: BlogEntity) => Blog.likedBy)
    likesCount: number;
  
    @OneToMany(
      type => CommentEntity,
      comment => comment.blog,
      { eager: true },
    )
    comments: CommentEntity;
  
    @ManyToOne(
      type => UserEntity,
      user => user.Blogs,
      { eager: true },
    )
    author: UserEntity;
  
    @BeforeInsert()
    generateSlug() {
      this.slug =
        slugify(this.title, { lower: true }) +
        '-' +
        ((Math.random() * Math.pow(36, 6)) | 0).toString(36);
    }
  
    toJSON() {
      return classToPlain(this);
    }
  
    toBlog(user?: UserEntity): any {
      let liked = null;
      if (user) {
        liked = this.likedBy.map(user => user.id).includes(user.id);
      }
      const Blog: any = this.toJSON();
      delete Blog.likedBy;
      return { ...Blog, liked };
    }
  }