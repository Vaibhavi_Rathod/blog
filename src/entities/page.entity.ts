import { classToPlain } from "class-transformer";
import { Entity, Column, BeforeInsert } from "typeorm";
import { AbstractEntity } from "./abstract-entity";
import * as slugify from "slug";

@Entity("Pages")
export class PageEntity extends AbstractEntity {
  @Column()
  uuid: string;

  @Column()
  title: string;

  @Column({type: 'longtext'})
  editor1: string;

  @Column({ default: false })
  published: boolean;

  @BeforeInsert()
  generateSlug() {
    this.uuid =
      slugify(this.title, { lower: true }) +
      "-" +
      ((Math.random() * Math.pow(36, 6)) | 0).toString(36);
  }

  toJSON() {
    return classToPlain(this);
  }
}
