import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import Stripe from "stripe";
import * as bcrypt from "bcrypt";
import { v4 } from "uuid";

const stripe = new Stripe(
  "sk_test_51IQRLbH483mbeU4KWrFCuBEvHfbdJeXQD6rf7qS0ouykf9ef30PJ2gKlBXEhcvg8XH6Okts213VwhlY4ENc6MSsl006WDK7IFm",
  {
    apiVersion: null,
  }
);

export default class CreateUsers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const customerParams = {
      email: "rathodvaibhavi18499@gmail.com",
      name: "Vaibhavi",
    };
    const customer = await stripe.customers.create(customerParams);
    const password = await bcrypt.hash('123456789', 10);
    const id = v4();
    await connection
      .createQueryBuilder()
      .insert()
      .into("users")
      .values([
        {
          userID: id,
          customer_id: customer.id,
          email: "rathodvaibhavi18499@gmail.com",
          username: "Vaibhavi",
          isAdmin: true,
          verified: true,
          password: password
        },
      ])
      .execute();
  }
}
