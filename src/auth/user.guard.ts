import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Res,
  UnauthorizedException,
} from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { InjectRepository } from "@nestjs/typeorm";
import { UserEntity } from "src/entities/user.entity";
import { Repository } from "typeorm";

var jwt = require("jsonwebtoken");
@Injectable()
export class UserGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    @InjectRepository(UserEntity) private userRepo: Repository<UserEntity>
  ) {}

  async canActivate(context: ExecutionContext): Promise<any> {
    try {
      const request = context.switchToHttp().getRequest();
      if (request.cookies.token) {
        const user = jwt.verify(request.cookies.token, "thisIsASecretKey");
        const find = await this.userRepo.findOne({
          where: { username: user.username, email: user.email },
        });
        if (find.isAdmin === true) {
          return true;
        }
      }
      const token = request.cookies.usertoken;
      const user = jwt.verify(token, "thisIsASecretKey");
      const find = await this.userRepo.findOne({
        where: { username: user.username, email: user.email },
      });
      if (find.verified === true) {
        return true;
      }
      // const user = request.user.user;
      return false;
    } catch {
      throw new UnauthorizedException();
    }
  }
}
