import { Controller, Post, Body, ValidationPipe, Res } from "@nestjs/common";

import { AuthService } from "./auth.service";
import { RegisterDTO, LoginDTO, AuthResponse } from "../models/user.model";
import { ResponseObject } from "src/models/response.model";

@Controller("users")
export class AuthController {
  constructor(private authService: AuthService) {}

  // SIGNUP
  @Post("/signup")
  async register(
    @Body(ValidationPipe) credentials: RegisterDTO, @Res() res
  ): Promise<ResponseObject<"user", AuthResponse>> {
    const user = await this.authService.register(credentials, res);
    return { user };
  }

  // LOGIN
  @Post("/login")
  async login(
    @Body(ValidationPipe) credentials: LoginDTO, @Res() res
  ): Promise<ResponseObject<"user", AuthResponse>> {
    const user = await this.authService.login(credentials, res);
    return { user };
  }
}
