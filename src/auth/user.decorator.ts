import {
  createParamDecorator,
  ExecutionContext,
  SetMetadata,
} from "@nestjs/common";

var jwt = require("jsonwebtoken");

export const User = createParamDecorator(
  async (data: unknown, ctx: ExecutionContext) => {
    const req = ctx.switchToHttp().getRequest();
    if (req.cookies.usertoken) {
      const user = jwt.verify(req.cookies.usertoken, "thisIsASecretKey");
      return user;
    }
    const token = req.cookies.token;
    const user = jwt.verify(token, "thisIsASecretKey");
    return user;
  }
);