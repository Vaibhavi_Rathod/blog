import {
  Injectable,
  Res,
  UnauthorizedException,
} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import * as bcrypt from "bcrypt";
import { UserEntity } from "src/entities/user.entity";
import { LoginDTO, RegisterDTO, AuthResponse } from "src/models/user.model";
import Stripe from "stripe";

const stripe = new Stripe(
  "sk_test_51IQRLbH483mbeU4KWrFCuBEvHfbdJeXQD6rf7qS0ouykf9ef30PJ2gKlBXEhcvg8XH6Okts213VwhlY4ENc6MSsl006WDK7IFm",
  {
    apiVersion: null,
  }
);

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity) private userRepo: Repository<UserEntity>,
    private jwtService: JwtService
  ) {}

  async register(credentials: RegisterDTO, @Res() res): Promise<any> {
    try {
      const user = this.userRepo.create(credentials);
      user.password = await bcrypt.hash(credentials.userID, 10);
      const customerParams = {
        email: user.email,
        name: user.username,
      };
      const customer = await stripe.customers.create(customerParams);
      user.customer_id = customer.id;
      await user.save();
      // const payload = { username: user.username, email: user.email };
      // const token = this.jwtService.sign(payload);
      // return { ...user.toJSON(), token };
      // const users = await this.userRepo.find();
      // return users ;
    } catch (error) {
      return res.render("exception.hbs", {
        message: error.message,
        status: error.status,
      });
    }
  }

  async login(
    { email, password }: LoginDTO,
    @Res() res
  ): Promise<AuthResponse> {
    try {
      const user = await this.userRepo.findOne({ where: { email } });
      if (!user) {
        throw new UnauthorizedException("Invalid Email");
      }
      const isValid = await user.comparePassword(password);

      if (!user || !isValid) {
        throw new UnauthorizedException("Invalid password");
      }
      const payload = { username: user.username, email: user.email };
      const token = this.jwtService.sign(payload);
      return { ...user.toJSON(), token };
    } catch (error) {
      return res.render("exception.hbs", {
        message: error.message,
        status: error.status,
      });
    }
  }
}
